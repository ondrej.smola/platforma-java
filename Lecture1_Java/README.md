


## Assignments

1. Setup [IntellijIDEA Ultimate]  (https://www.jetbrains.com/idea/)
2. Setup [JDK 17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)
3. Setup  [Apache Maven](https://maven.apache.org/)
4. Read about new features from [Java 8 to 17](https://openjdk.java.net/projects/jdk/)


# Exam questions
1. Describe 5 new features in Java since version 7
2. What is local type inference (Java 10)
3. Describe Java records in (Java 15)
3. Describe Java sealed classes introduced (Java 17)
4. Difference between Java and JVM
