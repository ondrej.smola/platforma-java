

# Spring Intro

1. Create Spring project using Maven
2. Change autowired dependency based on configuration variable (e.g. profile)
3. Write JUnit tests for autowired dependency
4. Compile, test and install project using Maven

# Exam questions

1. Describe dependency injection and inversion of control
2. Describe Spring application container
3. Describe annotations
   1. @Autowired
   2. @Component
   3. @Configuration
   4. @Qualifier
   5. @ComponentScan
4. What does bean represent in Spring
5. Describe difference between Singleton and Prototype bean scope
6. Describe bean lifecycle events
