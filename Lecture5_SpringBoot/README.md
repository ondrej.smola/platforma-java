

# Spring Boot

1. Update assignment 4 to use logging and Spring boot
2. Study one open source logging stack
   * Elasticsearch + Logstash + Kibana
   * GrafanaLabs Loki + Grafana
3. Study application containerization using Docker


# Exam questions

1. Describe annotations
   1. @SpringBootApplication
   2. @EnableAutoConfiguration
   3. @PropertySource
   4. @Profile
   5. @Value
2. Describe basic components of Logback logging system
3. Describe 3 logging levels with examples when to use them
4. Difference between SL4F and Logback
5. Describe one of open source logging stack
   1. ELK
   2. Grafana + Loki
6. Describe following application containerization terms
   1. Image
   2. Repository
   3. Container

