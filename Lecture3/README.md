


# Testing and project management

1. Create Maven project
2. Implement function that finds all anagrams in a string list
3. Write JUnit tests for this function
4. Compile, test and install project using Maven

# Exam questions

1. Describe difference between namespace, module and service 
2. Describe Maven POM
3. What is Super POM
4. Describe Maven build livecycle
5. Describe Maven goals
6. How are project dependencies managed by Maven

