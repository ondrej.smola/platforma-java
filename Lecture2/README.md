

**Write description** to your git repository!


# Garbage collectors
1. G1
   1. https://www.oracle.com/technetwork/tutorials/tutorials-1876574.html  
   2. https://www.youtube.com/watch?v=OhPGN2Av44E
2. ZGC
   1. https://www.baeldung.com/jvm-zgc-garbage-collector
   2. https://www.youtube.com/watch?v=88E86quLmQA


# Bytecode
1. https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html
2. https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-6.html
3. https://medium.com/swlh/an-introduction-to-jvm-bytecode-5ef3165fae70
4. https://www.youtube.com/watch?v=e2zmmkc5xI0


# Exam questions
1. Describe JVM heap and stack. Which variables are stored on heap and which on stack
2. How does GC work
   1. Stop the world 
   2. GC roots
3. Describe G1 collector
4. Describe ZGC collector
5. Compare G1 vs ZGC
6. Describe bytecode (groups, prefix/suffix, operand types)
7. How is bytecode generated and how can be viewed
8. Describe operand stack and local variables array
9. Describe how does bytecode interpretation works in runtime
10. What is JIT compilation, how does it work


